package btu.ge.exam

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.math.RoundingMode

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }




    private fun init() {

        btnCalc.setOnClickListener {
            converter()
        }

    }


    private fun converter() {
        if (userInputFahrenheit.text.isNotEmpty() && userInputCelsius.text.isNotEmpty()) {
            Toast.makeText(this, "შეიყვანეთ ან ცელსიუსი ან ფარენჰეიტი", Toast.LENGTH_LONG).show()
        } else if(userInputFahrenheit.text.isNotEmpty()) {
            val x = userInputFahrenheit.text.toString().toDouble()
            val calc = (x - 32) * 5 / 9
            val answer = calc.toBigDecimal().setScale(2, RoundingMode.HALF_EVEN).toDouble()
            result.text = answer.toString()
        } else if (userInputCelsius.text.isNotEmpty()) {
            val x = userInputCelsius.text.toString().toDouble()
            val calc = (x * 9 / 5) + 32
            val answer = calc.toBigDecimal().setScale(2, RoundingMode.HALF_EVEN).toDouble()
            result.text = answer.toString()
        } else {
            Toast.makeText(this, "შეავსეთ მინიმუმ ერთი ველი", Toast.LENGTH_LONG).show()
        }
    }


}
